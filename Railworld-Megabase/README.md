# Rail World 2022

![](railworld.jpg)

Image Album: https://lychee.homelab.zn80.net/#16472020946584

## Map Exchange String

```
>>>eNp1Uj0sQ1EUPsfToBIxdCFCB5tU4i8M0ntJRESwmQz68xpNqk9e
2wFDOxiJxcLCisRmsDWRCAmJMNmIxYBUCANJ3fPeu33Xw0nOed/9zv+
9DwGhD2xhAEXmr44ZkdT+3l4YgHNiBJGKAxSYVH/MWFjQzZBh6mpcXc
zMxfWQkfyR7dfT+vxiKBrJ6GoNbc7IqufapGmkvRV9mayR/slkTV3Pq
In1OTOSTubmvbmAzTP904WVViAt5yFYLpMKdCt2JQUsCMMBBSfFF0sl
EwmA4JDQYWqAiMuBg9G7pQ2Gdkwnd0DJYYpRyYxJMMn/dbVL0KvU6bH
kVQF206xo4UTVchfYzhVyIq49He58nBTD+LX7cjURnWXYNRJ4LnUfhI
W7hvasqpitTZIjuQrImrfMcd0wvDgneWToo4wAGT4gTHFcA2xsEGhnV
ZhgC8jRwrJMgGPCkne5yb0E18y7h7iIQSreSuaUjNXQncyGfJ0jb5Pe
JjdE5HeDOkPc3fBMtj1W+nsG+f0Q6h4epp3/8Qx+ahivmAetMo24z8s
aeeLbHDUCFPUmOPtk/XxOKfvbyK3r1sCVEoPPjqn8N2BPr/8=<<<
```

## Mods In Use

- Armored Biters
- FNEI
- Fill4Me
- Nanobots
- Squeak Through
- Vehicle Snap
- YARM
- alien-biomes
- even-distribution
- helmod (or factorio planner)
- informatron
- lightorio
