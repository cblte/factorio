# Factorio - Krastorio 2 Mod

![](factorio_CxcSHOlLpK.jpg)

My first try on [Krastorio 2](https://mods.factorio.com/mod/Krastorio2). Images can be gound here: https://lychee.homelab.zn80.net/#16471048009777

In addition to Krastorio 2 I am using the following mods:

- Alien Biomes
- Alien Biomes High-Res-Terrain
- Factorio Library
- Helmod
- Lightorio
- Squeak Through
- TinyStart
- VehicleSnap

## Map Exhcange String

```
>>>eNpjZGBkMGUAgwZ7EOZlSc5PzIHwGByAyAEokJMCkwZhruT8goLU
It38olRkdZzJRaUpqbr5mSi6uVLzUnMrdZMSi4GKHewhuMGeOSO/BNl
Mjszc1KLizBIUEzkyi/Lz0K3hzc3MSy1KzNEtTyxJLUKW4S5KLErVzU
0tScwpRhZnLS7Jz0Mxg7WkKDW1GNl+7tKixLzM0lx02xgYX4Qcbmtok
WMA4f/1DAr//4MwkPUAqAaEGRgbwKoZgWIwwJqck5mWxsCg4AjETmBF
jIzVIuvcH1ZNsWeEqNFzgDI+QEUOJMFEPGEMPwecUiowhgmSOcZg8Bm
JAbG0BGgFVBWHA4IBkWwBSTIy9r7duuD7sQt2jH9Wfrzkm5Rgz2joKv
Lug9E6O6AkO8gLTHBi1kwQ2AnzCgPMzAf2UKmb9oxnz4DAG3tGVpAOE
RCxoAdIHPBmZmAU4INxFWQYYE6zgxkj4sCYBgbfYD55DGNctkf3BzAg
bECGy4GIEyACbCHCZRBmx2EHxgYLRqisJEIJUL8RA7IbUhA+PAmz9jC
S/WgOwYwIZH+giag4YIkGLpCFKXDiBTPcNcDwvMAO4znMd2BkBjFAqr
4AxSA8SFKFGAWhBRzAwc3MgAAf7BnEQxkDAHhgtQ8=<<<
```

![](factorio_lyn9rqv8Wl.png)